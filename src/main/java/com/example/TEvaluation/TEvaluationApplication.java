package com.example.TEvaluation;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@EnableAutoConfiguration//启用自动配置
@SpringBootApplication(scanBasePackages = {"com.example.TEvaluation","intercepter"})//包扫描
@MapperScan("com.example.TEvaluation.mapper")//mapper扫描
public class TEvaluationApplication  {

	public static void main(String[] args) {
		SpringApplication.run(TEvaluationApplication.class, args);
	}

}
