package com.example.TEvaluation.mapper;

import com.example.TEvaluation.domain.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface UserMapper {

   /* @Select("select * from user")
    List<User> findall();*/

    /* @Select("select * from user where username=#{username}")
     User findall(@Param("username") String username);*/
     /*@Select("select * from us" )
     List<User> find();*/

     @Select("select * from user where userid=#{userid and password=#{password")
     User login(@Param("userid")Integer userid ,@Param("password") String password);
}
