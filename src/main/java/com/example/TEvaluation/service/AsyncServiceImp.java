package com.example.TEvaluation.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;


import java.util.Random;
import java.util.concurrent.Future;
@Service
public class AsyncServiceImp  implements  AsyncService{
    private  static Random random=new Random();
    @Override
    @Async
    public Future<String> doTask() throws Exception {
        System.out.println("任务一开始执行");
        long start=System.currentTimeMillis();
        Thread.sleep(random.nextInt(10000));
        long end=System.currentTimeMillis();
        System.out.println("任务一结束"+(end-start));
        return new AsyncResult<>("任务一结束");
    }

    @Override
    @Async
    public Future<String> doTask1() throws Exception {
        System.out.println("任务二开始执行");
        long start=System.currentTimeMillis();
        Thread.sleep(random.nextInt(10000));
        long end=System.currentTimeMillis();
        System.out.println("任务二结束"+(end-start));
        return new AsyncResult<>("任务2结束");
    }
    @Async
    @Override
    public Future<String> doTask2() throws Exception {
        System.out.println("任务三开始执行");
        long start=System.currentTimeMillis();
        Thread.sleep(random.nextInt(10000));
        long end=System.currentTimeMillis();
        System.out.println("任务三结束"+(end-start));
        return new AsyncResult<>("任务san结束");
    }
}
