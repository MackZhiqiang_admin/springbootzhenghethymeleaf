package com.example.TEvaluation.service;

import java.util.concurrent.Future;
//异步执行
public interface AsyncService {
    Future<String > doTask() throws  Exception;
    Future<String > doTask1() throws  Exception;
    Future<String > doTask2() throws  Exception;
}
