package com.example.TEvaluation.domain;

public class Result {
    private Integer resultid;

    private Integer sid;

    private String courseQuality;

    private String courseToUnderstand;

    private String eva;

    private Integer scorcount;

    private Integer tid;

    public Integer getResultid() {
        return resultid;
    }

    public void setResultid(Integer resultid) {
        this.resultid = resultid;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public String getCourseQuality() {
        return courseQuality;
    }

    public void setCourseQuality(String courseQuality) {
        this.courseQuality = courseQuality == null ? null : courseQuality.trim();
    }

    public String getCourseToUnderstand() {
        return courseToUnderstand;
    }

    public void setCourseToUnderstand(String courseToUnderstand) {
        this.courseToUnderstand = courseToUnderstand == null ? null : courseToUnderstand.trim();
    }

    public String getEva() {
        return eva;
    }

    public void setEva(String eva) {
        this.eva = eva == null ? null : eva.trim();
    }

    public Integer getScorcount() {
        return scorcount;
    }

    public void setScorcount(Integer scorcount) {
        this.scorcount = scorcount;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }
}