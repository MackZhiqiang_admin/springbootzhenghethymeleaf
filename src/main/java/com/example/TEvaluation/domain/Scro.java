package com.example.TEvaluation.domain;

public class Scro {
    private Integer scorid;

    private Integer scor;

    private Integer count;

    private Integer tercherid;

    public Integer getScorid() {
        return scorid;
    }

    public void setScorid(Integer scorid) {
        this.scorid = scorid;
    }

    public Integer getScor() {
        return scor;
    }

    public void setScor(Integer scor) {
        this.scor = scor;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getTercherid() {
        return tercherid;
    }

    public void setTercherid(Integer tercherid) {
        this.tercherid = tercherid;
    }
}