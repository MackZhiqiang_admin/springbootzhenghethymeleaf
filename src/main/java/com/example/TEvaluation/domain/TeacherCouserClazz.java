package com.example.TEvaluation.domain;

public class TeacherCouserClazz {
    private Integer teacherCouserid;

    private Integer teacherid;

    private Integer couserid;

    private Integer clazzid;

    public Integer getTeacherCouserid() {
        return teacherCouserid;
    }

    public void setTeacherCouserid(Integer teacherCouserid) {
        this.teacherCouserid = teacherCouserid;
    }

    public Integer getTeacherid() {
        return teacherid;
    }

    public void setTeacherid(Integer teacherid) {
        this.teacherid = teacherid;
    }

    public Integer getCouserid() {
        return couserid;
    }

    public void setCouserid(Integer couserid) {
        this.couserid = couserid;
    }

    public Integer getClazzid() {
        return clazzid;
    }

    public void setClazzid(Integer clazzid) {
        this.clazzid = clazzid;
    }
}