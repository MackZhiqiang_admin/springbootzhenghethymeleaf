package com.example.TEvaluation.domain;

public class Clazz {
    private Integer clazzid;

    private String clazzname;

    private Integer couserid;

    public Integer getClazzid() {
        return clazzid;
    }

    public void setClazzid(Integer clazzid) {
        this.clazzid = clazzid;
    }

    public String getClazzname() {
        return clazzname;
    }

    public void setClazzname(String clazzname) {
        this.clazzname = clazzname == null ? null : clazzname.trim();
    }

    public Integer getCouserid() {
        return couserid;
    }

    public void setCouserid(Integer couserid) {
        this.couserid = couserid;
    }
}