package com.example.TEvaluation.domain;

public class Couser {
    private Integer couserid;

    private String cousername;

    public Integer getCouserid() {
        return couserid;
    }

    public void setCouserid(Integer couserid) {
        this.couserid = couserid;
    }

    public String getCousername() {
        return cousername;
    }

    public void setCousername(String cousername) {
        this.cousername = cousername == null ? null : cousername.trim();
    }
}