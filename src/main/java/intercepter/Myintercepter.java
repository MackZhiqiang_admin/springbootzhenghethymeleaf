package intercepter;

import com.example.TEvaluation.contorller.IntercepterController;
import com.example.TEvaluation.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
/*自定义拦截器*/
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
@Configuration
public class Myintercepter implements   WebMvcConfigurer{


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
      HandlerInterceptor inter=new HandlerInterceptor() {
          @Override
          public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
              HttpSession session=request.getSession(true);
              System.out.println("自定义拦截器");

              Object username=session.getAttribute("userinfo");
           //拦截器

              /*if(username==null){
                  response.sendRedirect(request.getContextPath()+"/login.html");
                  System.out.println("登陆失败");
                  return false;

              }else {
                  System.out.println("登陆成功");
                  return  true;

              }*/
              return  true;


          }

          @Override
          public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

          }

          @Override
          public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

          }
      };
        registry.addInterceptor(inter).addPathPatterns("/**")
                .excludePathPatterns("/css/**","/js/**","/images/**")//排除样式、脚本、图片等资源文件
                .excludePathPatterns("/login.html");//排除登录页面
                /*.excludePathPatterns("/wechatplatformuser/defaultKaptcha")//排除验证码
                .excludePathPatterns("/wechatplatformuser/loginRBAC");//排除用户点击登录按钮*/


    }
}
